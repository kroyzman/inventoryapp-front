import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from '../models/Item';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': "application/json"
  })
}

@Injectable({
  providedIn: 'root'
})
export class InventoryService {


  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8080/inventory';

  getItems(page: number): Observable<Item[]> {
    const url = `${this.baseUrl}?page=${page}`;
    return this.http.get<Item[]>(url);
  }

  deleteItem(item: Item): Observable<any> {
    const url = `${this.baseUrl}/delete/${item.id}`
    return this.http.delete<Item>(url, httpOptions);
  }

  updateItem(item: Item): Observable<Item> {
    const url = `${this.baseUrl}/update/${item.id}`
    return this.http.put<Item>(url, item, httpOptions);
  }

  addItem(item: Item): Observable<Item> {
    const url = `${this.baseUrl}/add`;
    return this.http.post<Item>(url, item, httpOptions);
  }

  getItemsCollectionSize(): Observable<any> {
    const url = `${this.baseUrl}/items/size`;
    return this.http.get<any>(url);
  }
}
