export class Item {
    id?: number;
    name: string = '';
    amount: number = 0;
    inventoryCode: string = '';
}