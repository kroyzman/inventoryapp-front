import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Item } from 'src/app/models/Item';

@Component({
  selector: 'app-item-table-component',
  templateUrl: './item-table-component.component.html',
  styleUrls: ['./item-table-component.component.css']
})
export class ItemTableComponentComponent implements OnInit {
  @Input() items: Item[] = [];
  @Input() collectionSize: number = 0;
  @Output() deleteItem: EventEmitter<Item> = new EventEmitter();
  @Output() updateItem: EventEmitter<Item> = new EventEmitter();
  @Output() changePage: EventEmitter<number> = new EventEmitter();

  page: number = 1;
  pageSize: number = 5;

  constructor() {
  }

  ngOnInit(): void {
  }

  onDelete(item: Item) {
    this.deleteItem.emit(item);
  }

  onUpdate(item: Item) {
    this.updateItem.emit(item);
  }

  refreshItems() {
    this.changePage.emit(this.page - 1);
  }


}
