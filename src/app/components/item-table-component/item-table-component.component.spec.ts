import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemTableComponentComponent } from './item-table-component.component';

describe('ItemTableComponentComponent', () => {
  let component: ItemTableComponentComponent;
  let fixture: ComponentFixture<ItemTableComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemTableComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemTableComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
