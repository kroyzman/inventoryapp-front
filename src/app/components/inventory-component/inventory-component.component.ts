import { Component, OnInit, ViewChild } from '@angular/core';
import { Item } from 'src/app/models/Item';
import { InventoryService } from '../../services/inventory.service';
import { ItemTableComponentComponent } from '../item-table-component/item-table-component.component';

@Component({
  selector: 'app-inventory-component',
  templateUrl: './inventory-component.component.html',
  styleUrls: ['./inventory-component.component.css']
})
export class InventoryComponentComponent implements OnInit {
  @ViewChild(ItemTableComponentComponent) itemTable: ItemTableComponentComponent = Component.prototype;
  items: Item[] = [];
  collectionSize: number = 0;

  constructor(private inventoryService: InventoryService) { }

  ngOnInit(): void {
    this.getItems(0);
    this.inventoryService.getItemsCollectionSize().subscribe(size => this.collectionSize = size);
  }



  getItems(page: number) {
    this.inventoryService.getItems(page).subscribe(
      items => {
        this.items = items;
      }
    );
  }

  deleteItem(item: Item) {
    this.items = this.items.filter(i => item.id !== i.id);
    this.inventoryService.deleteItem(item).subscribe(() => {
      this.collectionSize--;
      this.getItems(this.itemTable.page - 1);
    });
  }

  updateItem(item: Item) {
    this.inventoryService.updateItem(item).subscribe();
  }

  onAddItem(item: Item) {
    this.inventoryService.addItem(item).subscribe(() => {
      this.collectionSize++;
      this.getItems(this.itemTable.page - 1);
    });

  }

}
