import { Component, OnInit, TemplateRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Item } from 'src/app/models/Item';

@Component({
  selector: 'app-add-item-modal',
  templateUrl: './add-item-modal.component.html',
  styleUrls: ['./add-item-modal.component.css']
})
export class AddItemModalComponent implements OnInit {
  @Output() onAddItem: EventEmitter<Item> = new EventEmitter();
  @ViewChild('content')
  private content: TemplateRef<any> = TemplateRef.prototype;
  name: string = "";
  quantity: number = 0;
  inventoryCode: string = "";

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  open() {
    this.modalService.open(this.content, { ariaLabelledBy: 'modal-basic-title' });
  }

  onSubmit(modal: any, addForm: NgForm) {
    if (addForm.valid) {
      const item = { "name": this.name, "amount": this.quantity, "inventoryCode": this.inventoryCode };
      this.onAddItem.emit(item);
      modal.close();
    }

  }



}
