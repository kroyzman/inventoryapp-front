import { Component, OnInit, TemplateRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Item } from 'src/app/models/Item';

@Component({
  selector: 'app-update-modal',
  templateUrl: './update-modal.component.html',
  styleUrls: ['./update-modal.component.css']
})
export class UpdateModalComponent implements OnInit {
  @Output() onUpdate: EventEmitter<Item> = new EventEmitter();
  @ViewChild('content')
  private content: TemplateRef<any> = TemplateRef.prototype;
  item: Item = Item.prototype;
  quantity: number = 0;

  constructor(private modalService: NgbModal) {

  }

  ngOnInit(): void {

  }

  open(item: Item) {
    this.item = item;
    this.quantity = this.item.amount;
    this.modalService.open(this.content, { ariaLabelledBy: 'modal-basic-title' })
  }

  onSubmit(modal: any, updateForm: NgForm) {
    if (updateForm.valid) {
      this.item.amount = this.quantity;
      this.onUpdate.emit(this.item);
      modal.close()
    }
  }

}
