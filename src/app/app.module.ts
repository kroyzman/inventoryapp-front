import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './components/layout/header/header.component';
import { ItemTableComponentComponent } from './components/item-table-component/item-table-component.component';
import { InventoryComponentComponent } from './components/inventory-component/inventory-component.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateModalComponent } from './components/update-modal/update-modal.component';
import { AddItemModalComponent } from './components/add-item-modal/add-item-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ItemTableComponentComponent,
    InventoryComponentComponent,
    UpdateModalComponent,
    AddItemModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
